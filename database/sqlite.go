package database

import (
	"database/sql"
	"fmt"
	"magneto/models"
	"math"

	_ "github.com/mattn/go-sqlite3"
)

var (
	DataBase *sql.DB
)

func SetupDB() {
	var errDB error
	DataBase, errDB = sql.Open("sqlite3", "./database/data/mutants.db")
	if errDB != nil {
		fmt.Println(errDB)
		panic(errDB)
	}
	initizalizeDB()
}

func initizalizeDB() {
	fmt.Println("Init Database.")
	stm, _ := DataBase.Prepare("CREATE TABLE IF NOT EXISTS dnas_verified (id INTEGER PRIMARY KEY, DNA TEXT ,isMutant BOOLEAN)")
	_, err := stm.Exec()
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}

func AddDNA(dna string, isMutant bool) {
	tx, _ := DataBase.Begin()
	stmt, _ := tx.Prepare("insert into dnas_verified (dna,isMutant) values (?,?)")
	_, err := stmt.Exec(dna, isMutant)
	if err != nil {
		fmt.Println(err)
	}
	tx.Commit()
}

func CountHumansAndMutants() (stat models.StatStruct) {
	err := DataBase.QueryRow("select SUM(CASE WHEN isMutant = true THEN 1 ELSE 0 END) as count_mutant_dna, SUM(CASE WHEN isMutant = false THEN 1 ELSE 0 END) as count_human_dna from dnas_verified").Scan(&stat.CountMutantDna, &stat.CountHumanDna)
	if err != nil {
		fmt.Println(err)
	}
	if stat.CountMutantDna > 0 && stat.CountHumanDna > 0 {
		stat.Ratio = math.Floor(float64(stat.CountMutantDna)/float64(stat.CountHumanDna)*100) / 100
	}
	return stat
}
