package cache

import (
	"fmt"
	"magneto/config"

	"github.com/gomodule/redigo/redis"
)

var (
	RedisConn redis.Conn
)

func SetupCache() {
	// Redigo Client
	var err error
	RedisConn, err = redis.Dial("tcp", config.REDIS_HOST)
	if err != nil {
		fmt.Println("Failed to connect to redis-server", config.REDIS_HOST)
	}
}
