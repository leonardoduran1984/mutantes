package controllers

import (
	"magneto/cache"
	db "magneto/database"
	"magneto/models"
	"strings"

	"github.com/gin-gonic/gin"
)

// Chequeo ADN para ver si es mutante
func CheckMutant(c *gin.Context) {
	var mutant models.Mutant
	c.BindJSON(&mutant)

	if mutant.DNA == nil {
		c.JSON(400, gin.H{
			"error": "Bad request",
		})
		return
	}

	// Arranco proceso de verificacion de DNA.
	/*
		Grabo secuencia de DNA en db.
	*/
	result := mutant.VerifyDNA()
	dnaString := strings.Join(mutant.DNA, ",")
	db.AddDNA(dnaString, result)

	// Purgo cache stats
	cache.RedisConn.Do("DEL", "stats")

	if !result {
		// Se retorna 403 porque no es mutante
		c.JSON(403, gin.H{
			"error": "Forbidden",
		})
		return
	}

	c.JSON(200, gin.H{
		"result": "OK",
		"info":   "Es mutante.",
	})
}
