package controllers

import (
	"encoding/json"
	"fmt"
	"magneto/cache"
	db "magneto/database"
	"magneto/models"

	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
)

func Stats(c *gin.Context) {
	var stat models.StatStruct

	// Me fijo si existe en redis si no traigo de db y cargo stats en redis
	s, err := redis.String(cache.RedisConn.Do("HGET", "stats", "JSON"))
	if err != nil {
		fmt.Println(err)
	}

	err = json.Unmarshal([]byte(s), &stat)
	if err == nil {
		c.JSON(200, stat)
		return
	}

	// Busco en db y cargo redis con stat
	stat = db.CountHumansAndMutants()
	b, err := json.Marshal(stat)
	if err != nil {
		return
	}
	_, err = cache.RedisConn.Do("HSET", "stats", "JSON", b)
	if err != nil {
		fmt.Println("Failed to JSONSet", err)
		return
	}
	c.JSON(200, stat)
}
