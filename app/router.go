package app

import (
	"magneto/controllers"
	"os"

	"github.com/gin-gonic/gin"
)

func SetupRouter() {
	// Este metodo levanta el router de gin gonic
	router := gin.New()
	router.Use(gin.Logger())

	/*
		Se setea puerto desde variable de entorno para heroku
		de no venir se setea puerto 9999.
	*/

	port := os.Getenv("PORT")

	if port == "" {
		port = "9999"
	}

	// Aqui colocamos las rutas
	router.POST("/mutant", controllers.CheckMutant)
	router.GET("/stats", controllers.Stats)

	router.Run(":" + port)
}
