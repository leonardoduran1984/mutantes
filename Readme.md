# Mutantes - ML

API para detectar si un humano es mutante basándose en su secuencia de ADN

## Instalación

* Descargar el repositorio


```bash
go get
```

## Ejecución

```bash
go run main.go
```

### EndPoints

* POST → /mutant/
{
“dna”:["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}

Si el DNA pertenece a un mutante, retorna un JSON {"info":"Es mutante.","result":"OK"}

En caso contrario, devuelve {"error":"Forbidden"}

* GET → /stats

Retorna un JSON con las estadísticas de las verificaciones de ADN

{
    "count_mutant_dna": 3,
    "count_human_dna": 7,
    "ratio": 0.42
}

## Tests

go test ./test

## Deploy

http://3.233.242.50:9999/stats

http://3.233.242.50:9999/mutant


## Autor

Leonardo Durán