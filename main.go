package main

import (
	"magneto/app"
	"magneto/cache"
	"magneto/database"
)

func main() {
	database.SetupDB() // Levanto DB
	cache.SetupCache() // Levanto cache Redis
	app.SetupRouter()  // Levanto gin gonic router
}
