package test

import (
	"fmt"
	"magneto/models"
	"testing"
)

func assertEqual(t *testing.T, a interface{}, b interface{}, message string) {
	if a == b {
		return
	}
	if len(message) == 0 {
		message = fmt.Sprintf("%v != %v", a, b)
	}
	t.Fatal(message)
}

func TestHorizontal1(t *testing.T) {
	var test1 = false
	var mutant models.Mutant
	mutant.DNA = append(mutant.DNA, "ATGCGA", "CAGATC", "TTTTGT", "ATAAGG", "TCCCTA", "TCACTG")

	// A T G C G A
	// C A G A T C
	//(T)T T(T)G T
	// A T A A G G
	// T C C C T A
	// T C A C T G

	test1 = mutant.VerifyDNA()
	assertEqual(t, test1, false, "Fallo control TestHorizontal1")
}

func TestHorizontal2(t *testing.T) {
	var test1 = false
	var mutant models.Mutant
	mutant.DNA = append(mutant.DNA, "ATGCGA", "CAGATC", "TTTTGT", "AAAAGG", "TCCCTA", "TCACTG")

	// A T G C G A
	// C A G A T C
	//(T)T T(T) G T
	//(A)A A(A) G G
	// T C C C T A
	// T C A C T G

	test1 = mutant.VerifyDNA()
	assertEqual(t, test1, true, "Fallo control TestHorizontal2")
}

func TestVertical1(t *testing.T) {
	var test1 = false
	var mutant models.Mutant
	mutant.DNA = append(mutant.DNA, "ATGCGA", "CAGATC", "TCTTGT", "ATAAGG", "TCCCTA", "TCACTG")

	// A T G C G A
	// C A G A T C
	// T C T T G T
	// A T A A G G
	// T C C C T A
	// T C A C T G

	test1 = mutant.VerifyDNA()
	assertEqual(t, test1, false, "Fallo control TestVertical1")
}

func TestVertical2(t *testing.T) {
	var test1 = false
	var mutant models.Mutant
	mutant.DNA = append(mutant.DNA, "ATGCGA", "CAGAGC", "TATTGT", "AAACGG", "TACCTA", "TCACTG")

	// A T G C(G)A
	// C(A)G A G C
	// T A T T G T
	// A A A C(G)G
	// T(A)C C T A
	// T C A C T G

	test1 = mutant.VerifyDNA()
	assertEqual(t, test1, true, "Fallo control TestVertical2")
}

func TestDiagonal1(t *testing.T) {
	var test1 = false
	var mutant models.Mutant
	mutant.DNA = append(mutant.DNA, "ATGCGA", "CAGATC", "TCATGT", "ATAAGG", "TCCCTA", "TCACTG")

	// A T G C G A
	// C A G A T C
	// T C A T G T
	// A T A A G G
	// T C C C T A
	// T C A C T G

	test1 = mutant.VerifyDNA()
	assertEqual(t, test1, false, "Fallo control TestDiagonal1")
}

func TestDiagonal2(t *testing.T) {
	var test1 = false
	var mutant models.Mutant
	mutant.DNA = append(mutant.DNA, "ATGCGA", "CAGATC", "CATTCT", "ACACGG", "TCCCTA", "TCACTG")

	// A T G C G A
	// C A G A T(C)
	//(C)A T T C T
	// A C A C G G
	// T C C C T A
	// T(C)A(C)T G

	test1 = mutant.VerifyDNA()
	assertEqual(t, test1, true, "Fallo control TestDiagonal2")
}

func TestCombineVH(t *testing.T) {
	var test1 = false
	var mutant models.Mutant
	mutant.DNA = append(mutant.DNA, "ATGCGA", "CAGATC", "CCTTTT", "ACACGG", "TCCCTA", "TCAGTG")

	// A T G C G A
	// C A G A T C
	// C(C T)T T(T)
	// A C A C G G
	// T C C C T A
	// T(C)A G T G

	test1 = mutant.VerifyDNA()
	assertEqual(t, test1, true, "Fallo control TestCombineVH")
}

func TestCombineVD(t *testing.T) {
	var test1 = false
	var mutant models.Mutant
	mutant.DNA = append(mutant.DNA, "ATGCGA", "CTGATC", "CCTATT", "ACATGG", "TCCCTA", "TCAGTG")

	// A T G C G A
	// C(T)G A T C
	// C(C)T A T T
	// A C A T G G
	// T C C C(T)A
	// T(C)A G T G

	test1 = mutant.VerifyDNA()
	assertEqual(t, test1, true, "Fallo control TestCombineVD")
}
