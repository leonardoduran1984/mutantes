package models

type StatStruct struct {
	CountMutantDna int     `json:"count_mutant_dna" db:"count_mutant_dna"`
	CountHumanDna  int     `json:"count_human_dna" db:"count_human_dna"`
	Ratio          float64 `json:"ratio" db:"ratio"`
}
