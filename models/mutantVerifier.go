package models

// Genero interface
type MutantVerifier interface {
	isMutant() bool
	horizontal() bool
	vertical() bool
	diagonal() bool
}

// DNA tendra el []string que viene del post.
type Mutant struct {
	DNA []string `json:"dna" db:"dna"`
}

// Esta funcion devuelve si es mutante o no (ver consigna)
func (mutant Mutant) isMutant() bool {
	// Creo channel para comunicarme con las go rutines y devolver el valor del chequeo
	chHorizontal := make(chan int)
	chVertical := make(chan int)
	chDiagonal := make(chan int)

	go mutant.horizontal(chHorizontal)
	go mutant.vertical(chVertical)
	go mutant.diagonal(chDiagonal)

	horizontal := <-chHorizontal
	vertical := <-chVertical
	diagonal := <-chDiagonal

	return (horizontal + vertical + diagonal) > 1
}

// Tipo de validaciones posibles
func (mutant Mutant) horizontal(ch chan int) {
	var lenArray = len(mutant.DNA[0])
	var rowNumber = 0
	var oldLetter = ""
	var c = ""
	adn_sequences := 0
	for rowNumber < lenArray {
		quantity := 1
		col_len := len(mutant.DNA[0])
		oldLetter = string(mutant.DNA[rowNumber][0])
		for index := 1; index < col_len; index++ {
			c = string(mutant.DNA[rowNumber][index])
			if c == oldLetter {
				quantity++
			} else {
				quantity = 1
			}
			if quantity == 4 {
				adn_sequences++
				quantity = 1
				break
			}
			oldLetter = c
		}
		// SI es mutante salgo del loop
		if adn_sequences > 1 {
			break
		}
		rowNumber++
	}
	ch <- adn_sequences
}

func (mutant Mutant) vertical(ch chan int) {
	var oldLetter = ""
	var c = ""
	adn_sequences := 0
	col_len := len(mutant.DNA[0])
	colNumber := 0
	for colNumber < col_len {
		quantity := 1
		oldLetter = string(mutant.DNA[0][colNumber])
		for index := 1; index < col_len; index++ {
			c = string(mutant.DNA[index][colNumber])
			if c == oldLetter {
				quantity++
			} else {
				quantity = 1
			}
			if quantity == 4 {
				adn_sequences++
				quantity = 1
				break
			}
			oldLetter = c
		}
		if adn_sequences > 1 {
			break
		}
		colNumber++
	}
	ch <- adn_sequences
}

func (mutant Mutant) diagonal(ch chan int) {
	// diagonal: verifica todas las posibles direcciones oblicuas  de la matriz la secuencia de 4 letras iguales

	maxSecuence := 4 // maxSecuence es la secuencia mínima de letras consecutivas iguales que tiene que tener para que sea considerado mutante
	var lenArray = len(mutant.DNA[0])
	var rowNumber = 0
	var oldLetter1 = "" // Variables terminadas en 1 para chequeo diagonal principal y triángulo superior
	var oldLetter2 = "" // Variables terminadas en 2 para chequeo triángulo inferior a la diagonal principal
	var oldLetter3 = "" // Variables terminadas en 3 para chequeo diagonal secundaria y triángulo superior
	var oldLetter4 = "" // Variables terminadas en 4 para chequeo triángulo inferior a la diagonal secundaria
	var c1 = ""         // c -> Variables que determinan la letra actual para compararla con la anterior
	var c2 = ""
	var c3 = ""
	var c4 = ""
	var quantity1 = 0 // Cantidad de veces que se repite una letra
	var quantity2 = 0
	var quantity3 = 0
	var quantity4 = 0

	var adn_sequences = 0

	// Itero sólo las diagonales que tienen al menos la cantidad maxSecuence para iterar
	for rowNumber <= lenArray-maxSecuence {
		// Por cada iteración, inicializo con la 1er letra de cada diagonal
		oldLetter1 = string(mutant.DNA[0][rowNumber])
		oldLetter2 = string(mutant.DNA[rowNumber][0])
		oldLetter3 = string(mutant.DNA[0][lenArray-1-rowNumber])
		oldLetter4 = string(mutant.DNA[lenArray-1-rowNumber][0])

		quantity1 = 0
		quantity2 = 0
		quantity3 = 0
		quantity4 = 0

		for i := 0; (i + rowNumber) < lenArray; i++ {
			c1 = string(mutant.DNA[i][i+rowNumber])
			c2 = string(mutant.DNA[i+rowNumber][i])
			c3 = string(mutant.DNA[i][lenArray-1-i-rowNumber])
			c4 = string(mutant.DNA[i+rowNumber][lenArray-1-i])

			if c1 == oldLetter1 {
				quantity1++
			} else {
				quantity1 = 1
			}

			if c3 == oldLetter3 {
				quantity3++
			} else {
				quantity3 = 1
			}

			// Si rowNumber = 0, está iterando la diagonal principal y la secundaria, y ya están consideradas en quantity1 y quantity3
			if rowNumber > 0 {
				if c2 == oldLetter2 {
					quantity2++
				} else {
					quantity2 = 1
				}

				if c4 == oldLetter4 {
					quantity4++
				} else {
					quantity4 = 1
				}
			}

			if quantity1 == maxSecuence {
				adn_sequences++
				quantity1 = 1
			}
			if quantity2 == maxSecuence {
				adn_sequences++
				quantity2 = 1
			}

			if quantity3 == maxSecuence {
				adn_sequences++
				quantity3 = 1
			}

			if quantity4 == maxSecuence {
				adn_sequences++
				quantity4 = 1
			}

			oldLetter1 = c1
			oldLetter2 = c2
			oldLetter3 = c3
			oldLetter4 = c4
			if adn_sequences > 1 {
				break
			}
		}
		rowNumber++
		if adn_sequences > 1 {
			break
		}
	}
	ch <- adn_sequences
}

// Expongo metodo de clase
func (mutant *Mutant) VerifyDNA() bool {
	return mutant.isMutant()
}
